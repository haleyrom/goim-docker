# Define the base image.
FROM centos:latest as builder
# Set environment variables.
ENV kafka_ver=2.12
ENV kafka_rel=2.6.3
ENV GO111MODULE on
ENV GOPROXY https://goproxy.io,direct
ENV DEPLOY_ENV dev
ENV REDIS_VERSION=6.0.5
ENV REDIS_URL=http://download.redis.io/releases/redis-$REDIS_VERSION.tar.gz
# Create dirs.
RUN cd /root && \
    mkdir src && \
    mkdir soft && \
    mkdir shell && \
    mkdir logs && \
    mkdir go && \
    mkdir go/src
# Add files
ADD shell /root/shell

# update source and install tools
# Install tools.
RUN yum update -y && \
    yum install -y bash sudo psmisc git go wget java-1.8.0-openjdk hg curl make gcc && \
    yum clean all && \
# Clone git \
    cd /root/src && \
    git clone https://github.com/Terry-Mao/goim.git && \
    git clone https://github.com/bilibili/discovery.git && \
    curl -L $REDIS_URL | tar xzv &&\
    cd redis-$REDIS_VERSION &&\
    make && \
    make install && \
# Download&Install Apache Kafka
    cd /root/soft && \
    wget https://dlcdn.apache.org/kafka/$kafka_rel/kafka_$kafka_ver-$kafka_rel.tgz && \
    tar -xzf kafka_$kafka_ver-$kafka_rel.tgz && \
    rm -rf kafka_$kafka_ver-$kafka_rel.tgz && \
    cd /root/soft/kafka_$kafka_ver-$kafka_rel && \
    mkdir /root/config && \
    mv ./config/zookeeper.properties /root/config/ && \
    ln -s /root/config/zookeeper.properties ./config/zookeeper.properties && \
    mv ./config/server.properties /root/config/ && \
    ln -s /root/config/server.properties ./config/server.properties && \
# Download the dependences. \
    cd /root/src && \
    \cp -rf discovery /root/go/src/ && \
    cd /root/go/src/discovery && \
    go mod tidy && \
    cd cmd/discovery && \
    mkdir /root/soft/discovery && \
    go build && \
    ls && \
    \cp -rf discovery /root/soft/discovery/ && \
    \cp -rf discovery.toml /root/config/discovery.toml && \
    ln -s /root/config/discovery.toml /root/soft/discovery/discovery.toml && \
    cd /root/src && \
    \cp -rf goim /root/go/src/ && \
    cd /root/go/src/goim && \
    go mod tidy && \
    make build && \
    cd target && \
    mkdir /root/soft/comet && \
    \cp -rf comet /root/soft/comet/ && \
    \cp -rf comet.toml /root/config/comet.toml && \
    ln -s /root/config/comet.toml /root/soft/comet/comet.toml && \
    mkdir /root/soft/logic && \
    \cp -rf logic /root/soft/logic/ && \
    \cp -rf logic.toml /root/config/logic.toml && \
    ln -s /root/config/logic.toml /root/soft/logic/logic.toml && \
    mkdir /root/soft/job && \
    \cp -rf job /root/soft/job/ && \
    \cp -rf job.toml /root/config/job.toml && \
    ln -s /root/config/job.toml /root/soft/job/job.toml && \
# Cleaning up
    yum autoremove -y git go wget && \
    rm -rf /root/src/go && \
# Permission setting up
    chmod -R 777 /root/shell && \
    ln -s /root/shell/start.sh /root/start.sh && \
    ln -s /root/shell/stop.sh /root/stop.sh

FROM centos:latest as prod
ENV REDIS_VERSION=6.0.5
COPY --from=builder /root/config /root/config
COPY --from=builder /root/soft /root/soft
COPY --from=builder /root/logs /root/logs
COPY --from=builder /root/shell /root/shell
COPY --from=builder /root/start.sh /root
COPY --from=builder /root/stop.sh /root
COPY --from=builder /usr/local/bin/redis* /usr/local/bin/

RUN yum install -y sudo java-1.8.0-openjdk libatomic && \
    yum clean all

CMD ["redis-server"]

# Volume settings
VOLUME ["/root/logs","/root/config"]

# Port settings
EXPOSE 2181
EXPOSE 3101
EXPOSE 3102
EXPOSE 3103
EXPOSE 3109
EXPOSE 3111
EXPOSE 3119
EXPOSE 6379
EXPOSE 7171
EXPOSE 7172
EXPOSE 9092

# Startup command
CMD /bin/bash -c /root/start.sh

