#!/bin/bash
echo "Set Env"
export REGION=sh
export ZONE=sh001
export DEPLOY_ENV=dev
export kafka_ver=2.12
export kafka_rel=2.6.3
nohup redis-server  2>&1 >> /root/logs/redis.log &
echo "Starting Zookeeper"
cd /root/soft/kafka_$kafka_ver-$kafka_rel/bin
nohup ./zookeeper-server-start.sh /root/config/zookeeper.properties 2>&1 >> /root/logs/zookeeper.log &
sleep 5
echo "Starting Kafka"
nohup ./kafka-server-start.sh /root/config/server.properties 2>&1 >> /root/logs/kafka.log &
sleep 5
echo "Creating Topic"
./kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 24 --topic KafkaPushsTopic
echo "Starting Discovery"
cd /root/soft/discovery
nohup ./discovery -conf /root/config/discovery.toml -log.dir="/root/logs/discovery.log" >/dev/null 2>&1 &
echo "Starting Logic"
cd /root/soft/logic
nohup ./logic -conf=/root/config/logic.toml  -region=sh -zone=sh001 -deploy.env=dev -weight=10 2>&1 >> /root/logs/logic.log &
sleep 5
echo "Starting Comet"
cd /root/soft/comet
nohup ./comet -conf=/root/config/comet.toml -region=sh -zone=sh001 -deploy.env=dev -weight=10 -addrs=127.0.0.1  2>&1 >> /root/logs/comet.log &
sleep 5
echo "Starting Job"
cd /root/soft/job
nohup sudo ./job -conf=/root/config/job.toml -region=sh -zone=sh001 -deploy.env=dev  2>&1 >> /root/logs/job.log &
sleep 5
while true;
do sleep 1;
done;
